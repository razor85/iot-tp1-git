/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iot.tp1;

//import com.alien.enterpriseRFID.reader.AlienClass1Reader;
import com.alien.enterpriseRFID.notify.ErrorMessage;
import com.alien.enterpriseRFID.notify.Message;
import com.alien.enterpriseRFID.notify.MessageListener;
import com.alien.enterpriseRFID.notify.MessageListenerService;
import com.alien.enterpriseRFID.reader.AlienClass1Reader;
import com.alien.enterpriseRFID.tags.Tag;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.prefs.Preferences;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author nightz
 */
public class MainPanel extends javax.swing.JPanel implements MessageListener {

    private final List<Tag> tags = new ArrayList();
    private final List<TagPanel> tagPanels = new ArrayList();
    private final MessageListenerService autoModeListener;
    private AlienClass1Reader reader = null;

    // Prevent messages from updating too fast.
    private long autoTimer = 0;
    private Color selectedPanelBg = null;
    private TagPanel selectedPanel = null;

    /**
     * Global Preferences.
     */
    public static class Settings {
        public String serverIp = "150.164.10.41";
        public String selfIP = "150.164.0.100";
        public String username = "alien";
        public String password = "password";
        public int port = 23;
        public int autoModeTime = 1000;
        public boolean isAutoMode = false;
    }

    // Default settings.
    private final Settings globalSettings = new Settings();

    /**
     * Creates new form MainPanel
     */
    public MainPanel() {
        initComponents();
        loadUserSettings();
        
        // Bind message Listener service to this listener.
        autoModeListener = new MessageListenerService(4000);

        try {
            autoModeListener.startService();
        } catch (Exception ex) {
            handleException(ex, "Failed to open read port: ");
        }

        methodComboBox.addItemListener((ItemEvent ie) -> {
            if (ie.getStateChange() == ItemEvent.SELECTED) {
                int selectedIndex = methodComboBox.getSelectedIndex();

                globalSettings.isAutoMode = (selectedIndex == 1);
                fetchButton.setEnabled(!globalSettings.isAutoMode);
                progressBar.setValue(0);

                try {
                    stopAll();
                    createReader();
                    if (globalSettings.isAutoMode)
                        setAutoMode();

                } catch (Exception ex) {
                    handleException(ex, "Failed to set auto mode: ");
                }
            }
        });

        fetchButton.addActionListener((ActionEvent evt) -> {
            try {
                fetchData();
            } catch (Exception ex) {
                handleException(ex, "Failed to fetch data: ");
            }
        });

        settingsButton.addActionListener((ActionEvent evt) -> {
            JDialog settingsWindow = new JDialog((Frame)null, "Settings", true);
            settingsWindow.setMinimumSize(new Dimension(300, 250));
            settingsWindow.setLocationRelativeTo(this);

            final Wrapper<Boolean> hadOk = new Wrapper<>(false);

            SettingsPanel settings = new SettingsPanel(globalSettings);
            settings.getOk().addActionListener((ActionEvent evt2) ->{
                settings.storePreferences();
                hadOk.set(true);
                settingsWindow.dispose();
            });

            settings.getCancel().addActionListener((ActionEvent evt2) ->{
                hadOk.set(false);
                settingsWindow.dispose();
            });

            settingsWindow.setContentPane(settings);
            settingsWindow.setVisible(true);

            // Use settings.
            if (hadOk.get()) {
                try {
                    storeSettings(settings.getSettings());
                    applyUserSettings();
                } catch (Exception ex) {
                    handleException(ex, "Failed to store user settings: ");
                }
            }
        });

        exportDataButton.addActionListener((ActionEvent ev) -> {
            try {
                exportData();
            } catch (Exception ex) {
                handleException(ex, "Failed to export tag data: ");
            }
        });
        
        try {
            createReader();
        } catch (Exception ex) {
            handleException(ex, "Failed to create reader: ");
        }
    }

    private void exportData() throws Exception {
        // Choose file to save
        JFileChooser chooser = new JFileChooser(new File("./"));
        if (chooser.showSaveDialog(this) != JFileChooser.APPROVE_OPTION)
            return;

        File filename = chooser.getSelectedFile();
        if (filename == null)
            return;

        BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
        for (Tag tag : tags) {
            String tagContents = dumpTag(tag);
            writer.write(tagContents);
            writer.write("\n");
        }

        writer.flush();
        writer.close();
    }

    private void loadUserSettings() {
        Preferences prefs = Preferences.userNodeForPackage(MainPanel.class);
        globalSettings.selfIP = prefs.get("selfAddress", "150.164.0.100");
        globalSettings.serverIp = prefs.get("address", "150.164.10.41");
        globalSettings.port = Integer.parseInt(prefs.get("port", "23"));
        globalSettings.username = prefs.get("username", "alien");
        globalSettings.password = prefs.get("password", "password");
        globalSettings.autoModeTime = Integer.parseInt(prefs.get("autoModeTime", "1000"));
    }

    private void storeSettings(Settings settings) {
        Preferences prefs = Preferences.userNodeForPackage(MainPanel.class);
        prefs.put("selfAddress", globalSettings.selfIP);
        prefs.put("address", globalSettings.serverIp);
        prefs.put("port", Integer.toString(globalSettings.port));
        prefs.put("username", globalSettings.username);
        prefs.put("password", globalSettings.password);
        prefs.put("autoModeTime", Integer.toString(globalSettings.autoModeTime));
    }

    private void stopAll() throws Exception {
        tags.clear();
        rebuildTagList();

        // Notify reader we don't want to receive auto mode messages
        // anymore, if there is any.
        if (reader != null) {
            if (!reader.isOpen())
                reader.open();

            reader.autoModeReset();
            reader.setAutoMode(AlienClass1Reader.OFF);
            reader.close();
        }

        reader = null;
        autoModeListener.setMessageListener(null);
    }

    private void applyUserSettings() throws Exception {
        // Figures if application is in Auto or Manual mode.
        final boolean isAutoMode = (methodComboBox.getSelectedIndex() == 1);

        stopAll();
        createReader();

        // Restart automatic mode if needed.
        if (isAutoMode)
            setAutoMode();
    }

    /**
     * Create manual reader.
     * 
     * @throws Exception If creation fails somehow.
     */
    private void createReader() throws Exception {
        reader = new AlienClass1Reader();
        reader.setConnection(globalSettings.serverIp,
          globalSettings.port);

        reader.setUsername(globalSettings.username);
        reader.setPassword(globalSettings.password);
        reader.open();
        reader.setTagListFormat(AlienClass1Reader.XML_FORMAT);
        reader.setNotifyFormat(AlienClass1Reader.XML_FORMAT);
        reader.autoModeReset();
        reader.setAutoMode(AlienClass1Reader.OFF);
        reader.close();
    }
    
    /**
     * Handle exception gracefully, showing a message when an error ocurrs.
     * 
     * @param ex Exception to be handled.
     * @param msg Message to display as a prefix to the error message.
     */
    private void handleException(Exception ex, String msg) {
        JOptionPane.showMessageDialog(this, msg + ex.getMessage(), "Error", 
          JOptionPane.ERROR_MESSAGE);

        ex.printStackTrace();
        System.out.println(msg + ex.getMessage());
    }

    /**
     * Update a tag panel by tag index.
     */
    private void updatePanel(int tagIndex) throws Exception {
        TagPanel panel = tagPanels.get(tagIndex);
        panel.updateVisual();
    }

    /**
     * Rebuild list of tags displayed by the interface.
     */
    private void rebuildTagList() throws Exception {
        tagPanel.removeAll();
        tagPanels.clear();

        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.insets = new Insets(0, 0, 0, 0);
        constraints.weightx = 1.0;

        for (Tag tag : tags) {
            final TagPanel newPanel = new TagPanel(tag);
            if (selectedPanelBg == null)
                selectedPanelBg = newPanel.getBackground();

            newPanel.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent evt) {
                    if (selectedPanel != null)
                        selectedPanel.setBackground(selectedPanelBg);

                    selectedPanel = newPanel;
                    selectedPanel.setBackground(Color.white);
                }
            });

            constraints.gridy++;
            tagPanel.add(newPanel, constraints);
            tagPanels.add(newPanel);
        }

        // Add special separator
        constraints.gridy++;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weighty = 1.0;
        tagPanel.add(new JPanel(), constraints);

        tagScrollPane.invalidate();
        tagScrollPane.revalidate();
        tagScrollPane.repaint();
    }
    
    /**
     * Configure reader device to report to listener.
     */
    private void configureReader() throws Exception {
        // Segundo a documentação, o IP deve ser atribuido pelo endereço de 
        // retorno da VPN, portanto não é possível que ele seja automático.
        reader.open();
        reader.setNotifyAddress(globalSettings.selfIP, autoModeListener.getListenerPort());
        reader.setAntennaSequence("0 1");
        reader.setNotifyFormat(AlienClass1Reader.XML_FORMAT);
        reader.setNotifyTrigger("TrueFalse"); // Notify whether there's a tag or not
        reader.setNotifyMode(AlienClass1Reader.ON);
        
        reader.autoModeReset();
        reader.setAutoStopTimer(globalSettings.autoModeTime);
        reader.setAutoMode(AlienClass1Reader.ON);
    }

    private void setAutoMode() throws Exception {
        autoTimer = System.currentTimeMillis();

        tags.clear();
        rebuildTagList();
        
        autoModeListener.setMessageListener(this);
        configureReader();
    }
    
    private void fetchData() throws Exception {
        reader.setAntennaSequence("0 1");
        reader.setTagListFormat(AlienClass1Reader.XML_FORMAT);
        reader.open();
        
        // Simulate progress bar
        progressBar.setValue(100);

        // Erase previous queries.
        tags.clear();
        
        Tag tagList[] = reader.getTagList();
        dumpTags(tagList);

        rebuildTagList();
    }

    private void dumpTags(Tag[] tagList) {
        if (tagList == null || tagList.length == 0) {
            JOptionPane.showMessageDialog(this, "No tags found.", 
              "No tags found", JOptionPane.INFORMATION_MESSAGE);

            return;
        } 

        System.out.println("Tag(s) found:");
        int numTags = tagList.length;
        for (int i = 0; i < numTags; ++i) {
            Tag tag = tagList[i];
            String tagContents = dumpTag(tag);
            System.out.println(tagContents);

            tags.add(tag);
        }
    }

    private String dumpTag(Tag tag) {
        StringBuilder builder = new StringBuilder();
        builder.append("ID: ").append(tag.getTagID()).append("\n")
        .append("Antenna: ").append(tag.getAntenna()).append("\n")
        .append("CRC: ").append(Long.toString(tag.getCRC())).append("\n")
        .append("First Seen: ").append( Long.toString(tag.getDiscoverTime())).append("\n")
        .append("Last Seen: ").append( Long.toString(tag.getRenewTime())).append("\n")
        .append("Read Count: ").append( Long.toString(tag.getRenewCount())).append("\n")
        .append("TTL: ").append( Long.toString(tag.getTimeToLive())).append("\n")
        .append("Speed: ").append( Double.toString(tag.getSpeed())).append("\n")
        .append("G2 Data: ").append( Arrays.toString(tag.getG2Data())).append("\n");

        return builder.toString();
    }

    /**
     * Search for a tag by using ID and antenna.
     */
    private int findTag(Tag otherTag) {
        for (int i = 0; i < tags.size(); ++i) {
            final Tag tag = tags.get(i);
            if (tag.getTagID().equals(otherTag.getTagID())
             && tag.getAntenna() == otherTag.getAntenna()) {

                return i;
            }
        }

        return -1;
    }

    @Override
    public synchronized void messageReceived(Message message) {
        if (message instanceof ErrorMessage) {
            JOptionPane.showMessageDialog(this, "Notify error in auto mode: " +
              message.getReason(), "Error", JOptionPane.ERROR_MESSAGE);

            StringBuilder builder = new StringBuilder();
            builder.append("Notify error from ")
              .append(message.getReaderIPAddress())
              .append(". Reason: ").append(message.getReason())
              .append(". Data read is: ").append(message.getXML());

            System.err.println(builder.toString());
            return;
        }

        if (message.getTagCount() == 0)
            return;

        try {
            final int tagListSize = tags.size();
            for (int i = 0; i < message.getTagCount(); i++) {
                Tag tag = message.getTag(i);
                int tagIndex = findTag(tag);
                if (tagIndex == -1) {
                    tags.add(tag);
                } else {
                    tags.set(tagIndex, tag);
                    updatePanel(tagIndex);
                }
            }

            // Added new tags?
            if (tagListSize != tags.size())
                rebuildTagList();

        } catch (Exception ex) {
            handleException(ex, "Failed to rebuild tag list.");
        }
    }

    public void closeAll() throws Exception {
        stopAll();
        if (autoModeListener != null)
            autoModeListener.stopService();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jLabel1 = new javax.swing.JLabel();
        methodComboBox = new javax.swing.JComboBox();
        fetchButton = new javax.swing.JButton();
        progressBar = new javax.swing.JProgressBar();
        tagScrollPane = new javax.swing.JScrollPane();
        tagPanel = new javax.swing.JPanel();
        settingsButton = new javax.swing.JButton();
        exportDataButton = new javax.swing.JButton();

        setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Collection method:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(jLabel1, gridBagConstraints);

        methodComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Manual", "Automatic" }));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE_LEADING;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 4, 0);
        add(methodComboBox, gridBagConstraints);

        fetchButton.setText("Fetch Data");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.BASELINE_LEADING;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 2);
        add(fetchButton, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 4, 0, 4);
        add(progressBar, gridBagConstraints);

        tagScrollPane.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

        tagPanel.setLayout(new java.awt.GridBagLayout());
        tagScrollPane.setViewportView(tagPanel);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 5;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        add(tagScrollPane, gridBagConstraints);

        settingsButton.setText("Settings");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(4, 2, 4, 4);
        add(settingsButton, gridBagConstraints);

        exportDataButton.setText("Export Data");
        exportDataButton.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new java.awt.Insets(4, 2, 4, 4);
        add(exportDataButton, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton exportDataButton;
    private javax.swing.JButton fetchButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JComboBox methodComboBox;
    private javax.swing.JProgressBar progressBar;
    private javax.swing.JButton settingsButton;
    private javax.swing.JPanel tagPanel;
    private javax.swing.JScrollPane tagScrollPane;
    // End of variables declaration//GEN-END:variables
}
