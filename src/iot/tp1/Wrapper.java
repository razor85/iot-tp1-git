package iot.tp1;

public class Wrapper<T> {
    private T value;

    public Wrapper(T defVal) {
        value = defVal;
    }

    public void set(T newValue) {
        value = newValue;
    }

    public T get() {
        return value;
    }
}
