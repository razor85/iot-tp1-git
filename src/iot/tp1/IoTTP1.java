package iot.tp1;

import static com.sun.java.accessibility.util.AWTEventMonitor.addWindowListener;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.UIManager;

/**
 * 
 * @author Romulo Fernandes
 */
public class IoTTP1 {

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            System.err.println("Failed to set native LNF: " + ex.getMessage());
        }

        MainPanel panel = new MainPanel();

        JFrame mainWindow = new JFrame("IoT TP1");
        mainWindow.setMinimumSize(new Dimension(800, 550));
        mainWindow.setLocationRelativeTo(null);
        mainWindow.setContentPane(panel);
        mainWindow.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        mainWindow.setVisible(true);

        mainWindow.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    panel.closeAll();
                } catch (Exception ex) {
                    System.err.println("Failed to close all connections: " +
                      ex.getMessage());
                }
            }
        });
    }
    
}
